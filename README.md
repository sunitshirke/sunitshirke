### :wave: Hey there

- 💼 Current CS Grad at [NJIT](https://www.njit.edu/)
- 🌎 I work remotely from Newark, NJ, USA
- 🦊 I previously worked at [MindCraft Software](https://www.mindcraft.in/), on the [Group Insurance Team at Kotak](https://customer.kotaklifeinsurance.com/kliportal/Selectlogin.aspx)
- 🕹 Check out my [recent side project]()
- 📄 Here's [my résumé]()
- 🌐 Check out my [personal website](), my [GitLab profile](https://gitlab.com/sunitshirke), and my [GitHub profile](https://github.com/Maddoxx88)
